# DeapSECURE Wiki

This repository contains wiki & shared group space for ALL DeapSECURE team members.
This repo is for the following purposes:

1. Shared wiki / knowledge base

2. Coordination notes

3. Other notes that don't belong to a particular learning module or subproject of DeapSECURE.

At this point in time, there is no need to clone this repository,
as the knowledge is not contained in the repo itself.
Go to Gitlab web interface and reach for the wiki, here:

https://gitlab.com/deapsecure/wiki/-/wikis/home
